/**}
*  Root Router
* Redirection to Routers 
*/

import express, { Request, Response } from 'express';
import helloRouter from './HelloRouter';
import { LogInfo } from '../utils/logger';
import usersRouter from './UserRouter';



// Primero hacemos una instancia del serve
let serve = express();


//Roter instance 
let rootRouter = express.Router();


//Activate for requet to http://localhost:8000/api
rootRouter.get('/', (req: Request, res: Response) => {
   LogInfo('Get: http://localhost:8000/api/')
   res.send('Welome to my API Restful: express, ts, Nodemos, Jest, swagger, etc...');
});

//Redirectio to Routers & Controllers
serve.use('/', rootRouter); // http://localhost:8000/api/
serve.use('/hello', helloRouter); // http://localhost:8000/api/hello ---> hello Router
serve.use('/users', usersRouter); // http://localhost:8000/api/users ---> users Router

//Add more routes to the app
export default serve;